#!/bin/bash

apt_install_gtest_dep(){
    apt-get update && apt-get install -y \
        cmake \
        g++ \
        gcovr \
        git
}

# Bash Strict Mode
set -eu -o pipefail

apt_install_gtest_dep
