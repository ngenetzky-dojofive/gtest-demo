#!/bin/bash

gtest_main(){
    cmake -H. -Bbuild
    cd build
    cmake --build .
    ctest
}

# Bash Strict Mode
set -eu -o pipefail

gtest_main
